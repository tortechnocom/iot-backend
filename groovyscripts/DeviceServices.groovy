import org.apache.ofbiz.base.config.GenericConfigException
import org.apache.ofbiz.base.util.Debug
import org.apache.ofbiz.base.util.UtilDateTime
import org.apache.ofbiz.base.util.UtilGenerics
import org.apache.ofbiz.base.util.UtilMisc
import org.apache.ofbiz.base.util.UtilValidate
import org.apache.ofbiz.entity.Delegator
import org.apache.ofbiz.entity.GenericEntityException
import org.apache.ofbiz.entity.GenericValue
import org.apache.ofbiz.entity.condition.EntityCondition
import org.apache.ofbiz.entity.condition.EntityOperator
import org.apache.ofbiz.entity.serialize.SerializeException
import org.apache.ofbiz.entity.serialize.XmlSerializer
import org.apache.ofbiz.entity.util.EntityQuery
import org.apache.ofbiz.entity.util.EntityUtilProperties
import org.apache.ofbiz.service.ModelService
import org.apache.ofbiz.service.ServiceUtil
import org.apache.ofbiz.service.ServiceValidationException
import org.apache.ofbiz.service.calendar.RecurrenceInfo
import org.apache.ofbiz.service.calendar.RecurrenceInfoException
import org.apache.ofbiz.service.config.ServiceConfigUtil
import org.apache.ofbiz.service.job.JobManagerException
import org.influxdb.BatchOptions
import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.influxdb.dto.Point
import org.influxdb.dto.Query

import javax.xml.bind.DatatypeConverter
import java.util.concurrent.TimeUnit

void checkResult(serviceResult, result) {
    result[ModelService.RESPONSE_MESSAGE] = serviceResult[ModelService.RESPONSE_MESSAGE]
    if (ModelService.RESPOND_ERROR.equals(serviceResult[ModelService.RESPONSE_MESSAGE])) {
        errorMessages = ""
        for (errorMessage in serviceResult[ModelService.ERROR_MESSAGE_LIST]) {
            if (UtilValidate.isEmpty(errorMessages)) {
                errorMessages = errorMessage
            } else {
                errorMessages = errorMessages + " " + errorMessage
            }
        }
        result[ModelService.ERROR_MESSAGE] = errorMessages
    } else {
        result[ModelService.SUCCESS_MESSAGE] = serviceResult[ModelService.SUCCESS_MESSAGE]
    }
}

def iotGetDeviceTypeList() {
    result = ServiceUtil.returnSuccess()
    deviceTypeList = []
    contentList = from("Content").where([contentTypeId: "IOT-DEVICE-TYPE"]).queryList()
    for (content in contentList) {
        deviceTypeList.add([
                deviceTypeId: content.contentId,
                name: content.contentName,
                description: content.description])
    }
    result.deviceTypeList = deviceTypeList
    return result
}

def iotGetDeviceIconList() {
    result = ServiceUtil.returnSuccess()
    deviceIconList = []
    contentList = from("Content").where([contentTypeId: "IOT-DEVICE-ICON"]).queryList()
    for (content in contentList) {
        deviceIconList.add([
                deviceIconId: content.contentId,
                description: content.description,
                name: content.contentName,
                selected: ""
        ])
    }
    result.deviceIconList = deviceIconList
    return result
}

def iotAddDevice() {
    result = [:]
    systemUserLogin = from("UserLogin").where("userLoginId", "system").queryOne()
    // check require fields
    if (deviceName) {
        try {
            serviceResult = dispatcher.runSync("createContent", [
                contentName: deviceName,
                description: description,
                ownerContentId: deviceTypeId,
                contentTypeId: "IOT-DEVICE",
                partyId: userLogin.partyId,
                roleTypeId: "OWNER",
                statusId: "DIVICE_CREATED",
                userLogin: systemUserLogin], 3600, true)
            checkResult(serviceResult, result)
            result = ServiceUtil.returnSuccess()
            result.deviceId = serviceResult.contentId
            return result
        } catch (ServiceValidationException e) {
            result = ServiceUtil.returnError(e.getMessage())
        }
    } else {
        result = ServiceUtil.returnError("Input data is missing!")
    }
}

def iotRegisterDevice() {
    result = [:]
    // check require fields
    if (deviceId) {
        message = ""
        content = from("Content").where([contentTypeId: "IOT-DEVICE", contentId: deviceId]).queryOne()
        if (content) {
            content.statusId = "DIVICE_ACTIVE"
            content.store()
            result = result = ServiceUtil.returnSuccess()
        } else {
            result = ServiceUtil.returnError(e.getMessage())
        }
    } else {
        result = ServiceUtil.returnError("Input data is missing!")
    }
    return result
}

def iotGetDevice() {
    result = ServiceUtil.returnSuccess()
    content = select("contentId", "contentTypeId", "ownerContentId",
                "contentName", "description", "serviceName")
            .from("Content")
            .where([contentId: deviceId, contentTypeId: "IOT-DEVICE"])
            .cache(true).queryFirst()
    if (content) {
        ownerContent = from("Content")
                .where([contentId: content.ownerContentId]).cache(true).queryOne()
        deviceIconId = "DEVICE-BULB"
        deviceIconName = "bulb"
        icon = from("ContentAttribute")
                .where([contentId: deviceId, attrName: "DEVICE-ICON"])
                .cache(true).queryOne()
        if (icon)  {
            iconValue = from("Content")
                    .where([contentId: icon.attrValue])
                    .cache(true).queryOne()
            deviceIconId = icon.attrValue
            if (iconValue) deviceIconName = iconValue.contentName
        }
        // temperature
        times = []
        values = []
        min = 0
        max = 0
        payload = ""
        if ((content.ownerContentId == "DEVICE-TEMPERATURE" || content.ownerContentId == "DEVICE-HUMIDITY") && isAll == "Y") {
            key = ""
            if (content.ownerContentId == "DEVICE-TEMPERATURE") {
                key = "temperature"
            } else if (content.ownerContentId == "DEVICE-HUMIDITY") {
                key = "humidity"
            }
            tempResult = runService("api-iotGetDeviceSensorDataList", [deviceId: deviceId, key: key, userLogin: userLogin])
            if (tempResult) {
                times = tempResult.times
                values = tempResult.values
                min = tempResult.min
                max = tempResult.max
            }
            payload = values.size() > 0 ? values.get(values.size() - 1) : ''
        } else {
            payload = content.serviceName
        }

        result.device = [
                deviceId: content.contentId,
                deviceName: content.contentName,
                description: content.description,
            deviceTypeId: content.ownerContentId,
                payload: payload,
                deviceIconId: deviceIconId,
                deviceIconName: deviceIconName,
                temperature: [times: times, values: values, min: min, max: max],
            mqtt: [topic: "iot/" + userLogin.userLoginId + "/" + ownerContent.contentName + "/" + deviceId]]
        return result
    } else {
        return ServiceUtil.returnError("Device not found!")
    }
}

def iotGetDeviceList() {
    result = ServiceUtil.returnSuccess()
    deviceList = []
    contentList = select("contentId").from("ContentAndRole")
            .where([contentTypeId: "IOT-DEVICE", partyId: userLogin.partyId])
            .filterByDate().orderBy("contentName").cache(true).queryList()
    for (content in contentList) {
        deviceResult = runService("api-iotGetDevice", [
                deviceId: content.contentId,
                isAll: "N",
                userLogin: userLogin
        ])
        deviceList.add(deviceResult.device)
    }
    result.deviceList = deviceList
    return result
}

def iotRemoveDevice() {
    result = ServiceUtil.returnSuccess()
    contentRoleList = from("ContentRole").where([contentId: deviceId, partyId: userLogin.partyId]).filterByDate().queryList()
    for (contentRole in contentRoleList) {
        contentRole.thruDate = UtilDateTime.nowTimestamp()
        contentRole.store()
    }
    return result
}

def iotUpdateDevice() {
    content = from("Content").where([contentTypeId: "IOT-DEVICE", contentId: deviceId]).queryOne()
    if (content) {
        content.contentName = deviceName
        content.description = parameters.description
        content.ownerContentId = deviceTypeId
        content.store()
        // Update Icon
        icon = from("ContentAttribute").where([contentId: deviceId, attrName: "DEVICE-ICON"]).queryOne()
        if (UtilValidate.isEmpty(icon)) {
            icon = delegator.makeValue("ContentAttribute")
            icon.set("contentId", deviceId)
            icon.set("attrName", "DEVICE-ICON")
            icon.set("attrValue", deviceIconId)
            icon.create()
        } else {
            icon.set("attrValue", deviceIconId)
            icon.store()
        }
        result = result = ServiceUtil.returnSuccess("Update device successful.")
    } else {
        result = ServiceUtil.returnError(e.getMessage())
    }
    return result
}

def iotGetMqttDetails() {
    result = ServiceUtil.returnSuccess()
    content = from("Content").where([contentId: deviceId]).queryOne()
    if (content) {
        result.uri = EntityUtilProperties.getPropertyValue("iot", "mqttServerUri", null, delegator)
        result.username = EntityUtilProperties.getPropertyValue("iot", "mqttUserName", null, delegator)
        result.password = EntityUtilProperties.getPropertyValue("iot", "mqttPassword", null, delegator)
        result.clientId = "iot-" + userLogin.partyId + "-" + UtilDateTime.nowTimestamp().getTime()
        ownerContent = from("Content").where([contentId: content.ownerContentId]).queryOne()
        result.topic = "iot/" + userLogin.userLoginId + "/" + ownerContent.mapKey + "/" + deviceId
        return result
    } else {
        return result = ServiceUtil.returnError("Device not found!")
    }
}

def iotSetDevicePayload() {
    content = from("Content").where([contentTypeId: "IOT-DEVICE", contentId: deviceId]).queryOne()
    if (content) {
        content.serviceName = payload
        content.store()
        if (content.udpIp && content.udpPort) {
            socket = new DatagramSocket()
            InetAddress address = InetAddress.getByName(content.udpIp)
            port = Integer.parseInt(content.udpPort)
            msg = deviceId + ",set," + content.serviceName
            println("=========== Sent UDP ...")
            println("UDP IP: " + content.udpIp)
            println("UDP Port : " + content.udpPort)
            println("UDP Data: " + msg)
            buf = msg.getBytes()
            packet = new DatagramPacket(buf, buf.length, address, port)
            socket.send(packet)
            socket.close()
        }
        result = result = ServiceUtil.returnSuccess()
    } else {
        result = ServiceUtil.returnError(e.getMessage())
    }
    return result
}

def iotGetGlobDevices() {
    result = ServiceUtil.returnSuccess()
    contentList = from("Content").where([contentTypeId: "IOT-DEVICE"]).queryList()
    result.devices = (int) contentList.size()
    return result
}

def iotGetDeviceScheduleSetting() {
    result = ServiceUtil.returnSuccess()
    deviceScheduleSetting = [:]
    frequencyList = from("Enumeration").where([enumTypeId: "SCHEDULE-FQ"]).queryList()
    typeList = from("Enumeration").where([enumTypeId: "SCHEDULE_TYPE"]).queryList()
    deviceScheduleSetting.frequencyList = frequencyList
    deviceScheduleSetting.typeList = typeList
    deviceScheduleSetting.actionList = [
        [actionId: "ON", description: "On"],
        [actionId: "OFF", description: "Off"]
    ]
    result.deviceScheduleSetting = deviceScheduleSetting
    //dispatcher.schedule(jobName, poolName, serviceName, serviceContext, startTime, frequency, interval, count, endTime, maxRetry);
    return result
}

def iotCreateDeviceSchedule() {
    result = ServiceUtil.returnSuccess()
    deviceScheduleLSetting = [:]
    Map<String, Object> serviceContext = new HashMap<String, Object>()
    serviceContext.deviceId = deviceId
    serviceContext.message = actionId
    serviceContext.userLogin = userLogin
    Calendar startCalendar = DatatypeConverter.parseDateTime(startDate)
    startCalendar.set(Calendar.SECOND, 0)
    startCalendar.set(Calendar.MILLISECOND, 0)
    startDateActual = startCalendar.getTimeInMillis()

    endDateActual = null
    if (parameters.endDate) {
        endDateActual = DatatypeConverter.parseDateTime(parameters.endDate).getTimeInMillis()
    }
    countActual = Integer.parseInt(count)
    intervalActual = Integer.parseInt(interval)
    frequencyActual = frequency ? Integer.parseInt(frequency) : null
    scheduleName = deviceId
    Debug.log("==> Available: " + dispatcher.getJobManager().isAvailable())
    job = schedule(scheduleName, 'pool', "api-iotDeviceMqttPublish",
            (java.util.Map) serviceContext, startDateActual, frequencyActual, intervalActual, countActual, endDateActual, 3, delegator)
    Debug.log("newJob: " + job)
    //schedule(scheduleName, 'pool', "iotMqttPublish", serviceContext, startDateActual, 1, 30, -1, endDateActual, 3, delegator);
    result.jobId = job.jobId
    return result
}

GenericValue schedule(String jobName, String poolName, String serviceName, HashMap context, Long startTime, Integer frequency, Integer interval,
                      Integer count, Long endTime, Integer maxRetry, Delegator delegator) throws JobManagerException {

    String dataId = null
    try {
        GenericValue runtimeData = delegator.makeValue("RuntimeData")
        runtimeData.set("runtimeInfo", XmlSerializer.serialize(context))
        runtimeData = delegator.createSetNextSeqId(runtimeData)
        dataId = runtimeData.getString("runtimeDataId")
    } catch (GenericEntityException ee) {
        throw new JobManagerException(ee.getMessage(), ee)
    } catch (SerializeException se) {
        throw new JobManagerException(se.getMessage(), se)
    } catch (IOException ioe) {
        throw new JobManagerException(ioe.getMessage(), ioe)
    }
    //assertIsRunning();
    // create the recurrence
    String infoId = null
    if (frequency > -1 && count != 0) {
        try {
            RecurrenceInfo info = RecurrenceInfo.makeInfo(delegator, startTime, frequency, interval, count)
            infoId = info.primaryKey()
        } catch (RecurrenceInfoException e) {
            throw new JobManagerException(e.getMessage(), e)
        }
    }
    // set the persisted fields
    if (UtilValidate.isEmpty(jobName)) {
        jobName = Long.toString((new Date().getTime()))
    }
    Map<String, Object> jFields = UtilMisc.<String, Object> toMap("jobName", jobName, "runTime", UtilDateTime.nowTimestamp(),
        "serviceName", serviceName, "statusId", "SERVICE_PENDING", "recurrenceInfoId", infoId, "runtimeDataId", dataId)
    // set the pool ID
    if (UtilValidate.isNotEmpty(poolName)) {
        jFields.put("poolId", poolName)
    } else {
        try {
            jFields.put("poolId", ServiceConfigUtil.getServiceEngine().getThreadPool().getSendToPool())
        } catch (GenericConfigException e) {
            throw new JobManagerException(e.getMessage(), e)
        }
    }
    // set the loader name
    jFields.put("loaderName", delegator.getDelegatorName())
    // set the max retry
    jFields.put("runTime", new java.sql.Timestamp(startTime))
    jFields.put("maxRetry", Long.valueOf(maxRetry))
    jFields.put("currentRetryCount", new Long(0))
    // create the value and store
    GenericValue jobV
    try {
        jobV = delegator.makeValue("JobSandbox", jFields)
        return delegator.createSetNextSeqId(jobV)
    } catch (GenericEntityException e) {
        throw new JobManagerException(e.getMessage(), e)
    }
}

def iotGetDeviceScheduleList() {
    result = ServiceUtil.returnSuccess()
    deviceScheduleList = []
    cond = EntityCondition.makeCondition(
            [
             EntityCondition.makeCondition("jobName", EntityOperator.EQUALS, deviceId),
             EntityCondition.makeCondition("parentJobId", EntityOperator.EQUALS, null)
            ], EntityOperator.AND)

    jobs = select("parentJobId", "recurrenceInfoId", "jobName", "jobId", "createdStamp", "runtimeDataId")
            .from("JobSandbox")
            .where(cond).distinct(true).orderBy("createdStamp").queryList()
    for (job in jobs) {
        recurrenceRule = from("RecurrenceRule").where("recurrenceRuleId", job.recurrenceInfoId).queryOne()
        description = ""
        frequency = ""
        if (recurrenceRule) {
            description = recurrenceRule.frequency + "/" + recurrenceRule.intervalNumber
            frequency = recurrenceRule.frequency
        } else {
            description = "ONCE/"
            frequency = "ONCE"
        }
            runtimeData = from("RuntimeData").where("runtimeDataId", job.runtimeDataId).queryOne()
        Debug.log("runtimeData: " + runtimeData.runtimeInfo)
        runTimeMap = UtilGenerics.checkMap(XmlSerializer.deserialize(runtimeData.getString("runtimeInfo"), delegator), String.class, Object.class);
        description = description + "/" + runTimeMap.message
        cond = EntityCondition.makeCondition([
                        EntityCondition.makeCondition("parentJobId", EntityOperator.EQUALS, job.jobId),
                        EntityCondition.makeCondition("statusId", EntityOperator.IN, ["SERVICE_PENDING","SERVICE_PENDING"])
                ], EntityOperator.AND)
        checkJobList1 = from("JobSandbox").where(cond).queryFirst()
        cond = EntityCondition.makeCondition([
                EntityCondition.makeCondition("jobId", EntityOperator.EQUALS, job.jobId),
                EntityCondition.makeCondition("statusId", EntityOperator.IN, ["SERVICE_PENDING","SERVICE_PENDING"])
        ], EntityOperator.AND)
        checkJobList2 = from("JobSandbox").where(cond).queryOne()
        enable = "N"
        Debug.log("==> checkJobList1: " + checkJobList1)
        Debug.log("==> checkJobList2: " + checkJobList2)
        runTime = ""
        startDateTime = ""
        if (UtilValidate.isNotEmpty(checkJobList1) || UtilValidate.isNotEmpty(checkJobList2)) {
            enable = "Y"
            if (UtilValidate.isNotEmpty(checkJobList1)) {
                runTime = checkJobList1.getString("runTime")
                startDateTime = checkJobList1.getString("startDateTime")
            } else {
                runTime = checkJobList2.getString("runTime")
            }
        } else {
            checkJobList1 = from("JobSandbox").where([parentJobId: job.jobId]).orderBy("-createdStamp").queryFirst()
            if (UtilValidate.isNotEmpty(checkJobList1)) {
                runTime = checkJobList1.getString("runTime")
                startDateTime = checkJobList1.getString("startDateTime")
            } else {
                checkJobList1 = from("JobSandbox").where([jobId: job.jobId]).orderBy("-createdStamp").queryFirst()
                runTime = checkJobList1.getString("runTime")
                startDateTime = checkJobList1.getString("startDateTime")
            }
        }
        Debug.log("==> enable:  " + enable)
        deviceScheduleList.add([jobId: job.jobId,
                                description: description,
                                startDateTime: startDateTime,
                                runTime: runTime,
                                enable: enable,
                                frequency: frequency
        ])
    }
    result.deviceScheduleList = deviceScheduleList
    return result
}

def iotDisableDeviceSchedule() {
    result = ServiceUtil.returnSuccess("Disable schedule successful.")
    deviceScheduleList = []
    jobs = from("JobSandbox").where([parentJobId: jobId]).queryList()
    for (job in jobs) {
        job.cancelDateTime = UtilDateTime.nowTimestamp()
        job.statusId = "SERVICE_CANCELLED"
        job.store()
    }
    job = from("JobSandbox").where([jobId: jobId]).queryOne()
    job.cancelDateTime = UtilDateTime.nowTimestamp()
    job.statusId = "SERVICE_CANCELLED"
    job.store()
    return result
}

def iotEnableDeviceSchedule() {
    result = ServiceUtil.returnSuccess("Enable schedule successful.")
    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
    String jobId = (String) context.get("jobId");
    GenericValue job;
    try {
        job = EntityQuery.use(delegator).from("JobSandbox").where("jobId", jobId).queryOne();
    } catch (GenericEntityException e) {
        Debug.logError(e, module);
        return ServiceUtil.returnError(e.getMessage());
    }
    nextTime = null;
    if (job) {
        if (UtilValidate.isNotEmpty(job.getString("recurrenceInfoId"))) {
            Debug.log("recurrenceInfoId: " + job.getString("recurrenceInfoId"))
            GenericValue ri = job.getRelatedOne("RecurrenceInfo", false);
            if (ri != null) {
                recurrence = new RecurrenceInfo(ri)
                expr = RecurrenceInfo.toTemporalExpression(recurrence);
                nextTime = expr.next(com.ibm.icu.util.Calendar.instance)
                Debug.log("nextTime: " + nextTime)
            }
        }
    }
    // update the job
    if (job != null) {
        job.set("statusId", "SERVICE_PENDING");
        job.set("startDateTime", null);
        job.set("finishDateTime", null);
        job.set("runTime", new java.sql.Timestamp(nextTime.getTimeInMillis()));
        job.set("cancelDateTime", null);
        job.set("runByInstanceId", null);

        // save the job
        try {
            job.store();
        } catch (GenericEntityException e) {
            Debug.logError(e, module);
            return ServiceUtil.returnError(e.getMessage());
        }
    }

    return result
//    newJob = delegator.makeValue("JobSandbox")
//    newJob.jobId = delegator.getNextSeqId("JobSandbox")
//    newJob.setNonPKFields(job)
//    newJob.finishDateTime = null
//    newJob.startDateTime = null
//    newJob.cancelDateTime = null
//    newJob.runTime = UtilDateTime.nowTimestamp()
//    newJob.statusId = "SERVICE_PENDING"
//    newJob.create()
//    runService("resetScheduledJob", [jobId: jobId, userLogin: systemUserLogin])
//    return result
}

def iotDeleteDeviceSchedule() {
    result = ServiceUtil.returnSuccess()
    deviceScheduleList = []
    jobs = from("JobSandbox").where([parentJobId: jobId]).queryList()
    for (job in jobs) {
        job.remove()
    }
    job = from("JobSandbox").where([jobId: jobId]).queryOne()
    if (job) {
        job.remove()
    }
    return result
}

def iotDeviceMqttPublish() {
    result = ServiceUtil.returnSuccess()
    iotGetDevice = runService("api-iotGetDevice", [deviceId: deviceId, userLogin: userLogin])
    iotMqttPublish = runService("api-iotMqttPublish", [topic: iotGetDevice.device.mqtt.topic, message: message, userLogin: userLogin])
    runService("api-iotSetDevicePayload", [deviceId: deviceId, payload: message, userLogin: userLogin])
    return result
}

def storeSensorData() {
    try {
        apiKey = from("ApiKey").where([
                apiKey: apiKey,
                apiSecret: apiSecret,
        ]).filterByDate().queryFirst()
        if (!apiKey) {
            return ServiceUtil.returnError("Your API Key is missing.")
        }
        if (!UtilValidate.isFloat(value)) {
            return ServiceUtil.returnError("Value is missing.")
        }
        url = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbURL", "", delegator)
        dbName = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbName", "", delegator)
        username = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbUsername", "", delegator)
        password = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbPassword", "", delegator)
        Debug.log("==> Store Sensor Data...")
        Debug.log("- deviceId: " + deviceId)
        Debug.log("- key: " + key)
        Debug.log("- value: " + value)
        Debug.log("- url: " + url)
        Debug.log("- dbName: " + dbName)
        Debug.log("- username: " + username)
        Debug.log("- password: " + password)
        InfluxDB influxDB = InfluxDBFactory
                .connect(url ,username, password)
        influxDB.setDatabase(dbName)
        influxDB.setRetentionPolicy("autogen")
        influxDB.enableBatch(BatchOptions.DEFAULTS)

        influxDB.write(Point.measurement(key)
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("deviceId", deviceId)
                .addField("value", value)
                .build())
        influxDB.close()
        // publish
        contentRole = from("ContentRole").where([contentId: deviceId]).queryOne()
        userLogin = null
        if (contentRole) {
            userLogin = from("UserLogin").where([partyId: contentRole.partyId]).queryFirst()
            content = from("Content").where([contentId: deviceId]).queryOne()
            content.serviceName = value
            content.store()
        }
        deviceResult = runService("api-iotGetDevice", ['deviceId': deviceId, userLogin: userLogin])
        if (deviceResult) {
            runService("api-iotMqttPublish",
                    [topic: deviceResult.device.mqtt.topic,
                     message: value,
                    userLogin: userLogin
                    ])
        }

    } catch (Exception e) {
        Debug.logError(e, null)
    }
    result = ServiceUtil.returnSuccess()
    return result
}

def getDeviceSensorDataList() {
    result = ServiceUtil.returnSuccess()
    try {
        url = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbURL", "", delegator)
        dbName = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbName", "", delegator)
        username = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbUsername", "", delegator)
        password = EntityUtilProperties.getPropertyValue("iot", "iotInfluxdbPassword", "", delegator)

        InfluxDB influxDB = InfluxDBFactory
                .connect(url
                , username
                , password)
        Query query = new Query("SELECT * FROM " + key + " WHERE (deviceId = '" + deviceId + "') AND time >= now() - " + duration
                , dbName)
        qResult = influxDB.query(query)
        times = []
        values = []
        float min = 0
        float max = 100
        index = 0
        if (UtilValidate.isNotEmpty(qResult.getResults()) && qResult.getResults().size() > 0) {
            if (qResult.getResults().get(0).getSeries() != null) {
                qResult.getResults().get(0).getSeries().get(0).values.each { value ->
                    if (UtilValidate.isFloat(value.get(2) + "")) {
                        times.add(value.get(0))
                        values.add(value.get(2))
                        valueInt = Float.parseFloat(value.get(2))
                        if (index == 0) {
                            min = valueInt
                            max = valueInt
                        } else {
                            if (valueInt < min) {
                                min = valueInt
                            }
                            if (valueInt > max) {
                                max = valueInt
                            }
                        }
                    }
                    index++
                }
            }
        }
        result.times = times
        result.values = values
        result.min = min
        result.max = max
        influxDB.close()
    } catch (Exception e) {
        Debug.logError(e, null)
    }
    return result
}

/*
* Deprecated: use getDeviceStatus
*
* */
def iotGetDevicesStatus() {
    result = ServiceUtil.returnSuccess()
    try {
        statusList = []
        deviceIdArray = deviceIds.split(",")
        systemUserLogin = from("UserLogin").where([
                userLoginId: "system"]).queryOne()
        Debug.log("==> deviceIds: "+ deviceIds)
        for (String deviceId : deviceIdArray) {
            Debug.log("==> deviceId: "+ deviceId)
            deviceResult = runService("api-iotGetDevice", [
                    deviceId: deviceId,
                    userLogin: systemUserLogin
            ])
            Debug.log("==> deviceResult: "+ deviceResult)
            statusList.add(deviceResult.device.payload)
        }
        result.statusList = statusList
    } catch (Exception e) {
        Debug.logError(e, null)
    }
    return result
}

def getDeviceConfig() {
    result = ServiceUtil.returnSuccess()
    try {
        apiKey = from("ApiKey").where([
                apiKey: apiKey,
                apiSecret: apiSecret,
        ]).filterByDate().queryFirst()
        if (!apiKey) {
            return ServiceUtil.returnError("Your API Key is missing.")
        }
        result.wifiName = apiKey.wifiName
        result.wifiPassword = apiKey.wifiPassword
        mqttUrl = EntityUtilProperties.getPropertyValue("iot", "mqttServerUrl", delegator)
        mqttPort = EntityUtilProperties.getPropertyValue("iot", "mqttServerPort", delegator)
        result.mqttServer = mqttUrl + ":" + mqttPort
        mqtt = from("MqttUser").where([username: apiKey.userLoginId]).queryFirst()
        if (mqtt) {
            result.mqttUser = mqtt.username
            result.mqttPassword = mqtt.password
        }
    } catch (Exception e) {
        Debug.logError(e, null)
    }
    return result
}

def getDeviceStatus() {
    result = ServiceUtil.returnSuccess()
    try {
        apiKey = from("ApiKey").where([
                apiKey: apiKey,
                apiSecret: apiSecret,
        ]).filterByDate().queryFirst()
        if (!apiKey) {
            return ServiceUtil.returnError("Your API Key is missing.")
        }
        statusList = []
        deviceIdArray = deviceIds.split(",")
        systemUserLogin = from("UserLogin").where([
                userLoginId: "system"]).queryOne()
        Debug.log("==> deviceIds: "+ deviceIds)
        for (String deviceId : deviceIdArray) {
            Debug.log("==> deviceId: "+ deviceId)
            deviceResult = runService("api-iotGetDevice", [
                    deviceId: deviceId,
                    userLogin: systemUserLogin
            ])
            Debug.log("==> deviceResult: "+ deviceResult)
            statusList.add(deviceResult.device.payload)
        }
        result.statusList = statusList
    } catch (Exception e) {
        Debug.logError(e, null)
    }
    return result
}

def setDeviceStatus() {
    result = ServiceUtil.returnSuccess()
    try {
        apiKey = from("ApiKey").where([
                apiKey: apiKey,
                apiSecret: apiSecret,
        ]).filterByDate().queryFirst()
        if (!apiKey) {
            return ServiceUtil.returnError("Your API Key is missing.")
        }
        statusList = []
        deviceIdArray = deviceIds.split(",")
        statusIdArray = statusIds.split(",")
        userLogin = from("UserLogin").where([
                userLoginId: apiKey.userLoginId]).queryOne()
        Debug.log("==> deviceIds: "+ deviceIds)
        count = 0
        for (String deviceId : deviceIdArray) {
            content = from("Content").where([contentId: deviceId]).queryOne()
            content.statusId = statusIdArray[count]
            content.store()

        }
        result.statusList = statusList
    } catch (Exception e) {
        Debug.logError(e, null)
    }
    return result
}

def updateDeviceStatus() {
    result = ServiceUtil.returnSuccess()
    try {
        apiKey = from("ApiKey").where([
                apiKey: apiKey,
                apiSecret: apiSecret,
        ]).filterByDate().queryFirst()
        if (!apiKey) {
            return ServiceUtil.returnError("Your API Key is missing.")
        }
        content = from("Content").where([contentId: deviceId]).queryOne()
        if (content) {
            content.udpIp = udpIp
            content.udpPort = udpPort
            content.store()
        }
    } catch (Exception e) {
        Debug.logError(e, null)
    }
    return result
}


def dialogflowAction() {
    if ("get".equals(queryResult.parameters.payload)) {
        Debug.log("==== Get device data")
        content = from("Content").where([contentId: queryResult.parameters.deviceId]).queryOne()
        postFix = ""
        if (content.ownerContentId == "DEVICE-TEMPERATURE") {
            postFix = " Celcius"
        } else if (content.ownerContentId == "DEVICE-HUMIDITY") {
            postFix = " Percent"
        }
        result = [
                fulfillmentText    : "I O T Bumblebee " + queryResult.queryText + " " + content.serviceName + postFix,
                fulfillmentMessages: []
        ]
    } else {
        try {
            runService("api-iotDeviceMqttPublish", [
                    deviceId: queryResult.parameters.deviceId,
                    message: queryResult.parameters.payload,
                    userLogin: userLogin])
        } catch (Exception e) {
            Debug.logError(e, null)
        }
        result = [
                fulfillmentText    : "I O T Bumblebee " + queryResult.queryText + " successful",
                fulfillmentMessages: []
        ]
    }
    return result
}

