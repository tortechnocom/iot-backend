import org.apache.ofbiz.base.crypto.BlowFishCrypt
import org.apache.ofbiz.base.util.UtilProperties
import org.apache.ofbiz.common.login.LoginServices

import javax.xml.bind.DatatypeConverter
import java.sql.ResultSet

import org.apache.http.util.EntityUtils
import org.apache.ofbiz.base.crypto.HashCrypt;
import org.apache.ofbiz.base.util.UtilDateTime;
import org.apache.ofbiz.base.util.UtilValidate
import org.apache.ofbiz.entity.util.EntityUtil
import org.apache.ofbiz.entity.util.EntityUtilProperties;
import org.apache.ofbiz.service.GenericServiceException;
import org.apache.ofbiz.service.ModelService;
import org.apache.ofbiz.service.ServiceUtil
import org.apache.ofbiz.service.ServiceValidationException

import ezvcard.property.Email;

void checkResult(serviceResult, result) {
    result[ModelService.RESPONSE_MESSAGE] = serviceResult[ModelService.RESPONSE_MESSAGE];
    if (ModelService.RESPOND_ERROR.equals(serviceResult[ModelService.RESPONSE_MESSAGE])) {
        errorMessages = "";
        for (errorMessage in serviceResult[ModelService.ERROR_MESSAGE_LIST]) {
            if (UtilValidate.isEmpty(errorMessages)) {
                errorMessages = errorMessage;
            } else {
                errorMessages = errorMessages + " " + errorMessage
            }
        }
        result[ModelService.ERROR_MESSAGE] = errorMessages;
    } else {
        result[ModelService.SUCCESS_MESSAGE] = serviceResult[ModelService.SUCCESS_MESSAGE];
    }
}
public iotGetUser() {
    result = ServiceUtil.returnSuccess();
    user = from("Person").where([partyId: userLogin.partyId]).queryOne()
    mqtt = from("MqttUser").where([username: userLogin.userLoginId]).queryFirst()
    user = user.getAllFields()
    mqtt = mqtt.getAllFields()
    mqtt.serverUrl = EntityUtilProperties.getPropertyValue("iot", "mqttServerUrl", null, delegator);
    mqtt.serverWebSocketPort = EntityUtilProperties.getPropertyValue("iot", "mqttServerWebSocketPort", null, delegator);
    mqtt.topic = "iot/" + userLogin.userLoginId + "/#";
    mqtt.secure = EntityUtilProperties.getPropertyValue("iot", "mqttSecure", null, delegator);
    result.mqtt = mqtt
    result.user = user
    return result;
}

public iotCreateAccount() {
    result = ServiceUtil.returnSuccess("Register account successful.")
    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
    checkCode = from("TrackingCode").where(["comments": email, description: registerCode]).queryFirst()
    if (UtilValidate.isNotEmpty(checkCode)) {
        checkCode = from("TrackingCode").where(["comments": email, description: registerCode]).filterByDate().queryFirst()
        if (UtilValidate.isEmpty(checkCode)) {
            return ServiceUtil.returnError("Register code is expired.")
        }
    }
    if (!UtilValidate.isEmail(email)) {
        return result = ServiceUtil.returnError("Your email is missing.")
    }
    serviceResult = runService("createPersonAndUserLogin", [currentPassword: currentPassword, currentPasswordVerify: currentPasswordVerify, userLoginId: email,
                                                            userLogin: systemUserLogin]);
    partyRole = runService("createPartyRole", [partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", userLogin: systemUserLogin])
    emailResult = runService("createPartyEmailAddress", [
            emailAddress: email, partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", contactMechTypeId: "EMAIL_ADDRESS",
            contactMechPurposeTypeId: "PRIMARY_EMAIL", userLogin: systemUserLogin])
    result.userLoginId = email

    //MQTT User
    mqttUserList = from("MqttUser").where().queryList()
    mqttUserId = 1;
    if (mqttUserList) mqttUserId = mqttUserList.size() + 1
    mqttUser = delegator.makeValue("MqttUser")
    mqttUser.id = mqttUserId
    println("===>" + email)
    mqttUser.username = email
    mqttUser.password = HashCrypt.getCryptedBytes("SHA", HashCrypt.CRYPT_CHAR_SET, currentPassword.getBytes())
    mqttUser.isSuperuser = 0
    mqttUser.create()
    // create mqtt acl
    mqttAclList = from("MqttAcl").where().queryList()
    mqttAclId = 1
    if (mqttAclList) mqttAclId = mqttAclList.size() + 1
    mqttAcl = delegator.makeValue("MqttAcl")
    mqttAcl.id = mqttAclId
    mqttAcl.username = email
    mqttAcl.access = 1
    mqttAcl.allow = 1
    mqttAcl.createdStamp = UtilDateTime.nowTimestamp()
    mqttAcl.topic = "iot/" + email + "/#";
    mqttAcl.create()
    return result
}
public generateApiKey() {
    result = ServiceUtil.returnSuccess()
    println("==> " + description)
    serviceResult = runService("api-generateApiKey",
            ["userLogin": userLogin, "description": description])
    apiKey = from("ApiKey").where([apiKeyId: serviceResult.apiKeyId]).queryOne()
    apiKey.wifiName = wifiName
    apiKey.wifiPassword = wifiPassword
    apiKey.description = description
    apiKey.store()
    result.apiKeyId = serviceResult.apiKeyId
    return result
}
public updateApiKey() {
    result = ServiceUtil.returnSuccess("Update Api Key successful.")
    apiKey = from("ApiKey").where([apiKeyId: apiKeyId]).queryOne()
    apiKey.description = description
    apiKey.wifiName = wifiName
    apiKey.wifiPassword = wifiPassword
    apiKey.store()
    return result
}