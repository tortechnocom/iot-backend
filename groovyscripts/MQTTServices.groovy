import java.util.Map;

import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilDateTime;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.util.EntityUtilProperties;
import org.apache.ofbiz.service.DispatchContext;
import org.apache.ofbiz.service.ServiceUtil;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public iotMqttPublish() {
    result = ServiceUtil.returnSuccess();
//    serverUrl = "192.168.1.101"
//    serverPort = "1883"
    serverUrl = EntityUtilProperties.getPropertyValue("iot", "mqttServerUrl", null, delegator)
    serverPort = EntityUtilProperties.getPropertyValue("iot", "mqttServerPort", null, delegator)
    mqttUser = from("MqttUser").where([username: userLogin.userLoginId]).queryFirst()
    userName = mqttUser.username
    password = mqttUser.password
    clientId = "ofbiz-" + UtilDateTime.nowTimestamp().getTime()
    try {
        MqttClient mqttClient = new MqttClient("tcp://" + serverUrl + ":" + serverPort, clientId);
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(userName);
        options.setPassword(password.toCharArray());
        options.setCleanSession(true);
        mqttClient.connect(options);
        MqttMessage mqttMessage = new MqttMessage(message.getBytes());
        mqttClient.publish(topic, mqttMessage);
        mqttClient.disconnect();
    } catch (MqttException e) {
        // TODO Auto-generated catch block
        Debug.logError("MQTT Error - " + e.getMessage(), module);
        e.printStackTrace();
    }
    return result;
}